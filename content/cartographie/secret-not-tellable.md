---
title: "Secret not tellable"
tags: ["Lycée", "SNT"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/bGZEB89ggGYBqc2/preview"
description: "Un jeu sérieux dans lequel les élèves doivent trouver un mot de passe respectant certaines contraintes … liées au programme de SNT"
---

* **[Site officiel](https://degrangem.forge.apps.education.fr/SecretNotTellable/)**
* [Source sur la forge](https://forge.apps.education.fr/DegrangeM/SecretNotTellable) ([ouvrir un ticket](https://forge.apps.education.fr/DegrangeM/SecretNotTellable/-/issues))
