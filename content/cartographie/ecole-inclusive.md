---
title: "École Inclusive"
tags: ["Collège"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/TqQnoa8at7NaJRw/preview"
description: "Une application libre pour la prise en charge des élèves en situation de handicap"
---

* **[Site officiel](https://linuxfr.org/news/ecole-inclusive-une-application-libre-pour-la-prise-en-charge-des-eleves-en-situation-de-handicap)**
* [Source sur la forge](https://forge.apps.education.fr/dsoulie/École_inclusive) ([ouvrir un ticket](https://forge.apps.education.fr/dsoulie/École_inclusive/-/issues))
