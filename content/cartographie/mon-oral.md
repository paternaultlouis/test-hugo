---
title: "Mon-oral"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/gFiqHW7NX3xE6X5/preview"
description: "Pratique de l'oral au primaire et au secondaire, préparation aux épreuves orales de collège et de lycée & création de commentaires audio pour les élèves"
---

* **[Site officiel](https://www.mon-oral.net/)**
* [Source sur la forge](https://forge.apps.education.fr/mon-oral) ([ouvrir un ticket](https://forge.apps.education.fr/mon-oral/-/issues))
