---
title: "La Forge des communs numériques éducatifs : collaborer, mutualiser, innover, Fidel Navamuel (Les Outils Tice), juin 2024"
type: "presse"
date: "2024-06-23"
---

[article] [La Forge des communs numériques éducatifs : collaborer, mutualiser, innover](https://outilstice.com/2024/06/forge-des-communs-numeriques-educatifs-collaborer-mutualiser), Fidel Navamuel (Les Outils Tice), juin 2024.
