---
title: "La Nuit Du Code"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/PRkKzr8Gb7qJcqE/preview"
description: "La Nuit du Code est un marathon de programmation durant lequel les élèves, par équipes de deux ou trois, ont 6h pour coder un jeu avec Scratch ou Python"
---

* **[Site officiel](https://www.nuitducode.net/)**
* [Source sur la forge](https://forge.apps.education.fr/nuit-du-code/) ([ouvrir un ticket](https://forge.apps.education.fr/nuit-du-code/-/issues))
