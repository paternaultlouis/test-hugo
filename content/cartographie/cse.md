---
title: "CSE"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/xRCL76gm3XDjkax/preview"
description: "Un outil pour créer un moteur de recherche personnalisé à partir d'une liste de sites"
---

* **[Site officiel](https://cse.forge.apps.education.fr/)**
* [Source sur la forge](https://forge.apps.education.fr/educajou/autobd) ([ouvrir un ticket](https://forge.apps.education.fr/cse/cse.forge.apps.education.fr/-/issues))
