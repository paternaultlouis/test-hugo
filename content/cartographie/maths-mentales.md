---
title: "Maths mentales"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/WodeNEeYBeLjQam/preview"
description: "Pour travailler les automatismes et le calcul mental en mathématiques"
---

* **[Site officiel](https://mathsmentales.net/)**
* [Source sur la forge](https://forge.apps.education.fr/mathsmentales/mathsmentales.forge.apps.education.fr) ([ouvrir un ticket](https://forge.apps.education.fr/mathsmentales/mathsmentales.forge.apps.education.fr/-/issues))
