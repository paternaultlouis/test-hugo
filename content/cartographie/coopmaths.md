---
title: "CoopMaths - Cahiers de vacances"
tags: ["Lycée", "mathématiques"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/dLaFxK3KyRFEGLj/preview"
description: "Cahiers de vacances destinés aux élèves qui vont entrer en seconde, en première ou en terminale, proposer un document libre et gratuit qui aide les élèves à entretenir en autonomie leur culture mathématique durant la trêve estivale"
---

* **[Site officiel](https://coopmaths.fr/www/cahiers-de-vacances/)**
* [Source sur la forge](https://forge.apps.education.fr/coopmaths/cahier-de-vacances) ([ouvrir un ticket](https://forge.apps.education.fr/coopmaths/cahier-de-vacances/-/issues))
