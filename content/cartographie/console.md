---
title: "Console de LaForgeEdu"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/NdcpeoxpCpXBNd7/preview"
description: "Outil de suivi en temps réel de l'activité de la forge"
---

* **[Site officiel](https://docs.forge.apps.education.fr/console/)**
* [Source sur la forge](https://forge.apps.education.fr/docs/console) ([ouvrir un ticket](https://forge.apps.education.fr/docs/console/-/issues))

