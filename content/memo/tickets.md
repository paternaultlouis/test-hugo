+++
title = 'Voir et utiliser les tickets'
+++

Les tickets sont un moyen d'interagir avec les personnes responsables d'un projet : signaler un problème, poser une question, etc.

Dans sa forme la plus simple, un ticket est une discussion portant sur un thème donné. Mais ils sont très riches, puisqu'ils peuvent inclure des métadonnées (pour les trier).

<!--more-->

## Afficher les tickets

La liste des tickets est affichée en cliquant sur « Tickets », dans le menu « Programmation ».

![Afficher les tickets](afficher.png)

### Liste des tickets

Cette liste alors les tickets avec leur titre, et quelques métadonnées.

![Liste des tickets](liste.png)

Remarquons qu'il est aussi possible d'afficher les tickets d'autres manières :

- cliquer sur un jalon (voir plus loin) affiche la liste des tickets associés ;
- cliquer sur une étiquette (voir plus loin) affiche la liste des tickets associés ;
- le tableau des tickets (voir plus loin) permet d'organiser ses tickets par listes par glisser-déposer ;
- pour les personnes connectées, la liste des tickets assignées est visible en haut à gauche ;
- etc.

    ![Boutons pour voir ses tickets et pense-bêtes](tickets-et-pense-bêtes.png)

### Page d'un ticket

La page d'un ticket permet de visualiser son titre, sa description et la discussion associée, ainsi que les métadonnées.

![Page d'un ticket](ticket.png)

Remarquons que les personnes connectées peuvent, depuis cette page, participer à la discussion, modifier ces métadonnées (si elles en ont l'autorisation), et s'abonner au ticket (pour recevoir des notifications à chaque évolution). Cette dernière fonctionnalité est accessible en déroulant le menu en cliquant sur les trois points à droite du titre.

![Suivre un ticket](ticket-options.png)

## Métadonnées

Plusieurs informations peuvent être associées à un ticket.

- Un jalon est une liste de tickets à fermer pour réussir un objectif (sortir la version 1 d'un logiciel par exemple). Chaque ticket ne peut être associé qu'à un seul jalon (ou aucun).

- Les étiquettes (ou labels, ou tags) sont des catégories associées à des tickets. Chaque ticket peut être associé à plusieurs étiquettes. Celles-ci peuvent être documentées avec une description.

- La date d'échéance désigne la date à laquelle le ticket devrait être résolu.

- La personne assignée est responsable du ticket. Ce ticket apparait dans le profil de cette personne, qui peut voir l'ensemble des tickets qui lui ont été ussignés.

- Les tâches sont des « sous-tickets », et permettent de découper un ticket (une tâche complexe) en plusieurs tâches plus simples, qui pourront être attribuées à des personnes différentes.

    ![Liste de tâches](tâches.png)

## Ouvrir un ticket

Ouvrir un ticket (pour poser une question, signaler un problème, etc.) se fait en cliquant sur le bouton « Nouveau ticket » dans la liste des tickets.

![Création d'un ticket](création-ticket.png)

Il est possible, à la création, de définir, outre un titre et une description, plusieurs informations. Pas de panique : seul le titre est obligatoire, et *toutes* ces informations peuvent être modifiées ultérieurement (y compris le titre).

- une personne assignée (qui est considérée comme responsable de ce tickets) ;
- un jalon (unique, qui est censé être clos un jour) ;
- des tags ou étiquettes (multiples) ;
- une date d'échéance ;
- etc.

La description est écrite en [markdown](https://forge.apps.education.fr/help/user/markdown) et peut [référencer d'autres éléments de la forge](https://forge.apps.education.fr/help/user/markdown#gitlab-specific-references) (autres tickets, projets, commits, etc.).

Les responsables de projet peuvent définir une [description par défaut](https://forge.apps.education.fr/help/user/project/description_templates.html) : Si le dépôt contient un fichier `.gitlab/issue_templates/default.md` (ou `.gitlab/merge_request_templates/default.md` pour les demandes de fusion), il sera utilisé comme description par défaut pour les nouveaux tickets. Ceci peut être utile pour indiquer à la personne créant le ticket quelles informations sont demandées.

## Référencer des tickets

Un ticket est identifié de manière unique est non modifiable par un numéro, qui peut être utilisé pour référencé ce ticket partout sur la forge.

### Depuis un ticket (ou n'importe quelle zone de texte de la forge)

Utiliser simplement #3 (numéro du ticket) dans une description quelconque.

### Depuis un commit

Lors d'un *commit* (validation), il est possible de [référencer un ticket](https://forge.apps.education.fr/help/user/project/issues/managing_issues.html#closing-issues-automatically), ou de le fermer automatiquement, ce qui permet de lier les tickets entre eux, ou de lier validations et tickets.

Par exemple, le *commit* suivant va référencer le ticket #3 (un lien vers ce commit apparaîtra dans la description de #3), et fermer le ticket #2 (avec un message « Fermé par le commit XXXXXXXXX » dans le ticket).

~~~
Ajout d'une partie « Contribuer » à la documentation

Closes #2
Utiliser le même modèle pour #3
~~~

## Courriel

Il est possible [d'ouvrir un ticket par courriel](https://docs.gitlab.com/ee/user/project/issues/create_issues.html#by-sending-an-email), à l'adresse électronique personnelle et confidentielle qui s'obtient en cliquant sur « Envoyer par courriel un(e) ticket à ce projet » en bas de la liste des tickets.
