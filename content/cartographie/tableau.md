---
title: "Tableau"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/2B7cGXi3m3by5wc/preview"
description: "Boîte à outils en ligne pour les disciplines scientifiques vouée à être projetée au tableau ou à être utilisée par les élèves sur leur propre ordinateur/tablette"
---

* **[Site officiel](https://tableau.ensciences.fr/)**
* [Source sur la forge](https://forge.apps.education.fr/thibaultgiauffret/tableau) ([ouvrir un ticket](https://forge.apps.education.fr/thibaultgiauffret/tableau/-/issues))
