---
title: "Flipbook"
tags: ["École"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/YSTA6o4EfaakjoR/preview"
description: "Outil pour créer facilement un livre numérique que l'on peut feuilleter en ligne"
---

* **[Site officiel](https://flipbook.forge.apps.education.fr/)**
* [Source sur la forge](https://forge.apps.education.fr/flipbook/flipbook.forge.apps.education.fr) ([ouvrir un ticket](https://forge.apps.education.fr/flipbook/flipbook.forge.apps.education.fr/-/issues))
