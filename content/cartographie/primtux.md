---
title: "PrimTux"
description: "Libre et gratuit, sécurisé, fonctionnant sur tous les ordinateurs, même anciens, PrimTux est système informatique complet dédié à l’apprentissage et conçu par des pédagogues, pour l’école et la maison."
featured_image: "https://primtux.fr/wp-content/uploads/2024/04/VirtualBox_Primtux8_07_03_2024_17_29_35-1-1024x640-1.png"
tags: ["École"]
---

* **[Site officiel](https://primtux.fr/)**
* [Source sur la forge](https://forge.apps.education.fr/primtux) ([ouvrir un ticket](https://forge.apps.education.fr/groups/primtux/-/issues))
