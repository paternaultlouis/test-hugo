---
title: "Lire couleur"
tags: ["École"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/fx5xPmR7fsiY7MD/preview"
description: "Ensemble d'outils destiné à aider les lecteurs débutants à décoder les mots en utilisant les principes de la lecture en couleur"
---

* **[Site officiel](https://lirecouleur.forge.apps.education.fr/weblc6/#/)**
* [Source sur la forge](https://forge.apps.education.fr/lirecouleur) ([ouvrir un ticket](https://forge.apps.education.fr/lirecouleur/-/issues))
