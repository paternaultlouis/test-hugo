---
title: "Face privacy"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/ry9xWrRqc7qeHB9/preview"
description: "* **[Site officiel](https://faceprivacy.forge.apps.education.fr/app/)**"
---
* [Source sur la forge](https://forge.apps.education.fr/faceprivacy/app) ([ouvrir un ticket](https://forge.apps.education.fr/faceprivacy/app/-/issues))
