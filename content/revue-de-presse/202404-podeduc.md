---
title: "Discours de cloture de la Journée du Libre Educatif 2024 à Créteil"
type: "presse"
date: "2024-04-03"
---

[vidéo] [Discours de cloture de la Journée du Libre Educatif 2024 à Créteil](https://podeduc.apps.education.fr/video/42285-journee-du-libre-educatif-a-luniversite-de-creteil/), (<a href='https://www.librealire.org/journee-du-libre-educatif-2024-audran-le-baron'>transcription</a>) avril 2024.
