---
title: "SituLearn"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/HT5xPSZqnn5dfqj/preview"
description: "SituLearn propose un ensemble d’applications interconnectées (SituLearn-Editor, SituLearn-Player et SituLearn-Monitor) qui permettent aux enseignants de créer leurs applications mobiles pour leurs sorties scolaires et de visualiser la position et la progression de leurs élèves pendant ces sorties."
---

* **[Site officiel](https://situlearn.univ-lemans.fr/)**
* [Source sur la forge](https://forge.apps.education.fr/lium/situlearn) ([ouvrir un ticket](https://forge.apps.education.fr/lium/situlearn/-/issues))
