---
title: "MyMarkmap"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/88RT75TYNSYsega/preview"
description: "Outil pour créer facilement et sans inscription des cartes mentales en ligne"
---

* **[Site officiel](https://mymarkmap.forge.apps.education.fr/)**
* [Source sur la forge](https://forge.apps.education.fr/myMarkmap/myMarkmap.forge.apps.education.fr) ([ouvrir un ticket](https://forge.apps.education.fr/myMarkmap/myMarkmap.forge.apps.education.fr/-/issues))
