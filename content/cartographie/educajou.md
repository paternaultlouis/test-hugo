---
title: "Educajou"
tags: ["École"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/mr5SxjDHCnjLkPx/preview"
description: "Suite d'applications libres pour l'école primaire"
---

* **[Site officiel](https://qcmcam.net/)**
* [Source sur la forge](https://forge.apps.education.fr/educajou) ([ouvrir un ticket](https://forge.apps.education.fr/educajou/-/issues))
