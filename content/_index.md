---
title: "Bienvenue sur la Forge des communs numériques éducatifs"
---

_Proposant informations et documentations sur la forge, ce site, est le point d'entrée du projet._

La forge des communs numériques éducatifs regroupe les enseignants, communautés d'enseignants et leurs partenaires qui créent et partagent des logiciels et ressources éducatives libres. 

Elle rassemble à la fois les professeurs développeurs qui y déposent le code de leurs projets (tels que PrimTux, MathALÉA, e-comBox ou La Nuit du Code) mais aussi les professeurs qui choisissent le format texte ou texte formaté (Markdown, LaTeX, etc.) pour éditer et publier du contenu pédagogique.

 Elle repose sur le logiciel libre GitLab. Elle mutualise et valorise les productions des communautés et aide certains de ses projets à passer à l’échelle.

[Se connecter / accéder à la forge](https://forge.apps.education.fr/users/sign_in)

## Que trouve-t-on sur la forge ?

- On trouve plein de projets, en vrac : [Explorer les projets](https://forge.apps.education.fr/explore)
- Voici quelques projets mis en avant : [Cartographie](forge.md#je-decouvre-cartographie-des-projets-de-la-forge)

## Comment lire cette documentation ?

Cette documentation peut se lire de deux manière :

1. de façon linéaire :

    1. [Qu'est-ce que la Forge ?](forge)
    2. [Premiers pas](premiers-pas)

2. en picorant ce qui vous intéresse, dans la partie précédente, ou dans les [mémos](memo) et [tutoriels](tuto), ou en utilisant la fonction de recherche en haut de cette page.

## Des questions ? Envie de discuter ?

- Si vous le souhaitez vous pouvez vous inscrire sur notre forum Tchap dédié à la forge pour y présenter vos projets et participer à sa communauté.

  [Salon Forges des communs numériques éducatifs](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.agriculture.tchap.gouv.fr)

- Vous pouvez également vous inscrire à DEV Forge, forum Tchap d'entraide technique, d'expertise et de développement des projets de la forge.

  [Salon DEV Forges des communs numériques éducatifs](https://tchap.gouv.fr/#/room/!BXZZsyWklktciNEDbM:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.intradef.tchap.gouv.fr)

- Vous pouvez aussi nous envoyer un courriel à forge-communs-numeriques@ldif.education.gouv.fr

- Alternativement, vous pouvez nous signaler les soucis rencontrés sur https://forge.apps.education.fr/docs/support ou par courriel forge-apps+guichet+docs-support-623-issue-@phm.education.gouv.fr


## On parle de LaForgeEdu

Voici les derniers liens qui mentionnent la forge. [Voir toute la revue de presse](revue-de-presse).

{{< presse last=3 >}}
