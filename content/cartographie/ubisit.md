---
title: "Ubisit"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/qNPiXfsGJo2A8qS/preview"
description: "Créez vos plans de classe et optimisez l’organisation de votre salle de cours"
---

* **[Site officiel](https://ubisit.forge.apps.education.fr/ubisit-plan-de-classe-aleatoire/)**
* [Source sur la forge](https://forge.apps.education.fr/ubisit/ubisit-plan-de-classe-aleatoire) ([ouvrir un ticket](https://forge.apps.education.fr/ubisit/ubisit-plan-de-classe-aleatoire/-/issues))
