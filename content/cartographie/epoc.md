---
title: "ePoc"
tags: ["Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/cqMy9CyrX7RNcf7/preview"
description: "Créer et partager librement votre formation sur mobile, ePoc est composé d’un éditeur permettant la création de contenu pédagogique et d’une application mobile permettant de suivre la formation produite"
---

* **[Site officiel](https://epoc.inria.fr/)**
* [Source sur la forge](https://forge.apps.education.fr/dsoulie/École_inclusivehttps://forge.apps.education.fr/epoc/e-poc) ([ouvrir un ticket](https://forge.apps.education.fr/epoc/e-poc/-/issues))
