---
title: "Markpage"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/Wi5kta2px6ZkHsG/preview"
description: "Outil qui permet de créer facilement un mini site web ou une application pour smartphone, à partir d'un simple fichier en Markdown"
---

* **[Site officiel](https://markpage.forge.apps.education.fr/)**
* [Source sur la forge](https://forge.apps.education.fr/markpage/markpage.forge.apps.education.fr) ([ouvrir un ticket](https://forge.apps.education.fr/markpage/markpage.forge.apps.education.fr/-/issues))
