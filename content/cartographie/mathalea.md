---
title: "MathALÉA"
tags: ["mathématiques", "École", "collège", "lycée"]
featured_image: "https://pedagogie.ac-orleans-tours.fr/fileadmin/_processed_/5/7/csm_Exemple_page_MathAl%C3%A9a_ccb431882a.jpg"
description: "Ressources libres en mathématiques pour vidéoprojecteur, smartphone et papier (plus de 2000 exercices à données aléatoires avec paramétrage des variables didactiques pour travailler les automatismes, pour imprimer des devoirs différents à chaque élève, pour évaluer les élèves sur ordinateur, pour donner des liens de révisions avec corrections)."
---

* **[Site officiel](https://coopmaths.fr/alea/)**
* [Source sur la forge](https://forge.apps.education.fr/coopmaths/mathalea/) ([ouvrir un ticket](https://forge.apps.education.fr/coopmaths/mathalea/-/issues))
