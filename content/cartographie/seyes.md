---
title: "Seyes"
tags: ["École"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/AcmprJAmet6eJYN/preview"
description: "Affiche une page quadrillée de type cahier d'écolier et permet d'y ajouter du texte en écriture cursive reliée"
---

* **[Site officiel](https://educajou.forge.apps.education.fr/seyes/)**
* [Source sur la forge](https://forge.apps.education.fr/educajou/seyes) ([ouvrir un ticket](https://forge.apps.education.fr/educajou/seyes/-/issues))
