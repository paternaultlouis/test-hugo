---
title: "Planet Alert"
tags: ["Collège", "anglais"]
featured_image: "https://minio.apps.education.fr/codimd-prod/uploads/upload_e04e08e23098db4d7f24dc0e0b023836.png"
description: "Un monde ludique pour une pédagogie stimulante qui articule le monde numérique (site internet, exercices en ligne, espace de classe en ligne) et le monde réel de la classe"
---

* **[Site officiel](https://planetalert.tuxfamily.org/)**
* [Source sur la forge](https://forge.apps.education.fr/flenglish/planetalert) ([ouvrir un ticket](https://forge.apps.education.fr/flenglish/planetalert/-/issues))
