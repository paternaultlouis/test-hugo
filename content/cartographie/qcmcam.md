---
title: "QCMCam"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/EX3fSAXrG47EGC5/preview"
description: "Outil en ligne qui permet d’avoir un retour élève lors d’une évaluation, d’un sondage ou d’un vote, chaque élève disposant d’un marqueur imprimé qui lui est propre, suivant l’orientation 4 réponses sont possibles"
---

* **[Site officiel](https://qcmcam.net/)**
* [Source sur la forge](https://forge.apps.education.fr/qcmcam/qcmcam2) ([ouvrir un ticket](https://forge.apps.education.fr/qcmcam/qcmcam2/-/issues))
