---
title: "Forges de l’ESR – Définition, usages, limitations rencontrées et analyse des besoins"
type: "presse"
date: "2023-05-23"
---

[article] [Forges de l’ESR – Définition, usages, limitations rencontrées et analyse des besoins](https://www.ouvrirlascience.fr/forges-de-lesr-definition-usages-limitations-rencontrees-et-analyse-des-besoins/), Daniel Le Berre, Jean-Yves Jeannas, Roberto Di Cosmo, François Pellegrini, Ouvrir la Science, mai 2023.
