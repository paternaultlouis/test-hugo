---
title: "e-comBox"
tags: ["Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/9tQtpCBRJDosZ3M/preview"
description: "Plateforme regroupant des applications métiers essentielles pour développer les compétences numériques des élèves et des étudiants."
---

* **[Site officiel](https://www.reseaucerta.org/pgi/e-combox)**
* [Source sur la forge](https://forge.apps.education.fr/e-combox/) ([ouvrir un ticket](https://forge.apps.education.fr/groups/e-combox/-/issues))
