---
title: "Auto BD"
tags: ["École", "Collège"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/oi68n7Qw7AeJLEK/preview"
description: "Un générateur en ligne de bandes dessinées"
---

* **[Site officiel](https://educajou.forge.apps.education.fr/autobd/)**
* [Source sur la forge](https://forge.apps.education.fr/educajou/autobd) ([ouvrir un ticket](https://forge.apps.education.fr/educajou/autobd/-/issues))
