---
title: "CréaCarte"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/LP4nFgtsAK6NgW6/preview"
description: "Outil en ligne pour concevoir et éditer des cartes à imprimer (école, collège, lycée)"
---

* **[Site officiel](https://flipbook.forge.apps.education.fr/)**
* [Source sur la forge](https://forge.apps.education.fr/lmdbt/creacarte) ([ouvrir un ticket](https://forge.apps.education.fr/lmdbt/creacarte/-/issues))
