---
title: "ChatMD"
tags: ["École", "Collège", "Lycée"]
featured_image: "https://nuage07.apps.education.fr/index.php/s/CcPmCMpynmWnB6p/preview"
description: "Un chatbot que vous pouvez configurer par vous-même en Markdown"
---

* **[Site officiel](https://eyssette.forge.apps.education.fr/chatMD/)**
* [Source sur la forge](https://forge.apps.education.fr/eyssette/chatMD) ([ouvrir un ticket](https://forge.apps.education.fr/eyssette/chatMD/-/issues))
